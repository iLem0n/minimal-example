package test.de;

import io.fabric8.kubernetes.client.KubernetesClient;
import io.javaoperatorsdk.operator.api.*;
import io.javaoperatorsdk.operator.api.Context;
import io.javaoperatorsdk.operator.processing.event.EventSourceManager;

@Controller
public class FlinkSessionController implements ResourceController<FlinkSession> {

    private final KubernetesClient client;

    public FlinkSessionController(KubernetesClient client) {
        this.client = client;
    }

    // TODO Fill in the rest of the controller

    @Override
    public void init(EventSourceManager eventSourceManager) {
        // TODO: fill in init
    }

    @Override
    public UpdateControl<FlinkSession> createOrUpdateResource(
        FlinkSession resource, Context<FlinkSession> context) {
        // TODO: fill in logic

        return UpdateControl.noUpdate();
    }
}

